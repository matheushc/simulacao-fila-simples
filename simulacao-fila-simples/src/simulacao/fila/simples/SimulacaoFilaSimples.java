package simulacao.fila.simples;

import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

public class SimulacaoFilaSimples {
    
    public static Map<String, String> obterHashMap(Scanner ler, String nome) {
        Map<String,String> params = new HashMap<String, String>();
        int tempo;
        System.out.print(""
                + "1 - Constante\n"
                + "2 - Uniforme\n"
                + "3 - Triangular\n"
                + "4 - Exponencial\n"
                + "5 - Normal\n"
                + "Informe a distribuicao desejada para o " + nome + ": ");
        tempo = ler.nextInt();

        String minimo;
        String maximo;
        String moda;
        String media;
        String variancia;

        switch (tempo) {
            case 1: //Constante
                System.out.print("Informe o " + nome + ": ");
                tempo = ler.nextInt();
                params.put("dist", "constante");
                params.put("valor", Integer.toString(tempo));
                break;
            case 2: //Uniforme
                System.out.print("Informe o valor inicial do intervalo: ");
                minimo = ler.next();
                System.out.print("Informe o valor final do intervalo: ");
                maximo = ler.next();
                params.put("dist", "uniforme");
                params.put("minimo", minimo);
                params.put("maximo", maximo);
                break;
            case 3: // Triangular
                System.out.print("Informe o valor inicial do intervalo: ");
                minimo = ler.next();
                System.out.print("Informe o valor final do intervalo: ");
                maximo = ler.next();
                System.out.print("Informe o valor da moda: ");
                moda = ler.next();
                params.put("dist", "triangular");
                params.put("minimo", minimo);
                params.put("maximo", maximo);
                params.put("moda", moda);
                break;
            case 4: //Exponencial
                System.out.print("Informe a média: ");
                media = ler.next();
                params.put("dist", "exponencial");
                params.put("media", media);
                break;
            case 5: //Normal
                System.out.print("Informe o valor da média: ");
                media = ler.next();
                System.out.print("Informe o valor da variância: ");
                variancia = ler.next();
                params.put("dist", "normal");
                params.put("media", media);
                params.put("variancia", variancia);
                break;
        }
        return params;
        
    }
    
    public static void main(String[] args) {
        
        Scanner ler = new Scanner(System.in);
        Map<String,String> paramsTec;
        Map<String,String> paramsTs = new HashMap<String, String>();
        
        System.out.print("Informe o tempo de simulação em segundos: ");
        int tempoLimite = ler.nextInt();
        
        paramsTec = obterHashMap(ler, "Tempo entre Chegadas (TEC)");
        paramsTs = obterHashMap(ler, "Tempo de Serviço (TS)");
        
        System.out.print("Informe a capacidade do sistema: ");
        int capacidade = ler.nextInt();
        
        System.out.print("A fila terá um limite de tamanho? (s/n)");
        String limite = ler.next();
        
        int tamanhoFila = 0;
        if (limite.equals("s")) {
            System.out.print("Informe o tamanho limite da fila: ");
            tamanhoFila = ler.nextInt();
        }
        
        new ControleSimulacao(tempoLimite, paramsTec, paramsTs, capacidade, tamanhoFila);
    }
}
