package simulacao.fila.simples;

public class Entidade {
    private final String identificador;
    private final double horaChegada;
    private double horaInicioAtendimento;
    private double horaFinalAtendimento;
    private double horaSaida;
    
    // tenho que setar:
    // tempo inicio de atendimento
    // tempo final de atendimento
    // tempo de saída do sistema
    
    public Entidade(String identificador, double horaChegada) {
        this.identificador = identificador;
        this.horaChegada = horaChegada;
        this.horaInicioAtendimento = -1;
        this.horaFinalAtendimento = -1;
        this.horaSaida = -1;
    }

    public String getIdentificador() {
        return identificador;
    }

    public double getHoraChegada() {
        return horaChegada;
    }

    public void setHoraInicioAtendimento(double horaInicioAtendimento) {
        this.horaInicioAtendimento = horaInicioAtendimento;
    }
    
    public double getHoraInicioAtendimento() {
        return horaInicioAtendimento;
    }
    
    public void setHoraFinalAtendimento(double horaFinalAtendimento) {
        this.horaFinalAtendimento = horaFinalAtendimento;
    }
    
    public double getHoraFinalAtendimento() {
        return horaFinalAtendimento;
    }
    
    public void setHoraSaida(double horaSaida) {
        this.horaSaida = horaSaida;
    }
    
    public double getHoraSaida() {
        return horaSaida;
    }

    public double getTempoFila() {
        return this.horaInicioAtendimento - this.horaChegada;
    }
    
    public double getTempoAtendimento() {
        return this.horaFinalAtendimento - this.horaInicioAtendimento;
    }
    
    public double getTempoSistema() {
        return this.horaSaida - this.horaChegada;
    }
}
