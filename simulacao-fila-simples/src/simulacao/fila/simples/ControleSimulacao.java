package simulacao.fila.simples;

import java.util.ArrayList;
import java.util.Map;

public class ControleSimulacao {
    private int tempo;
    private boolean parada;
    private ArrayList<Entidade> fila;
    private ArrayList<Entidade> chegaram;
    private ArrayList<Entidade> sairam;
    private int capacidadeSistema;
    private int tamanhoMaximoFila;
    private Entidade emAtendimento;
    private int quantidadeMaximaEntidadeSistema;

    public ControleSimulacao(
            int tempoLimite, Map<String,String> paramsTec,
            Map<String,String> paramsTs, int capacidadeSistema, int tamanhoMaximoFila) {
        this.tempo = 0;
        this.parada = false;
        this.fila = new ArrayList();
        this.chegaram = new ArrayList();
        this.sairam = new ArrayList();
        this.capacidadeSistema = capacidadeSistema;
        this.emAtendimento = null;
        this.quantidadeMaximaEntidadeSistema = 0;
        this.tamanhoMaximoFila = tamanhoMaximoFila;
        
        int tec = (int)obterTempo(paramsTec);
        int ts = (int)obterTempo(paramsTs);
        
        int contTec = 0;
        int contTs = 0;
        while (!this.parada) {
            System.out.println("\n\n#--------------------------------#" + 
                    "\nTempo: " + this.tempo);
            if (contTec == tec || this.tempo == 0) {
                tec = (int)obterTempo(paramsTec);
                System.out.print("Criando a entidade \"#" + this.tempo + "\" ");
                if (this.podeCriarNovaEntidade()) {
                    if (this.emAtendimento instanceof Entidade) {
                        System.out.println("na fila");
                        if (podeCriarNovaEntidadeNaFila()){
                            Entidade novaEntidade = new Entidade("#" + this.tempo, this.tempo);
                            this.chegaram.add(novaEntidade);
                            this.fila.add(novaEntidade);
                        } else {
                            break;
                        }
                    } else {
                        System.out.println("no atendimento");
                        this.emAtendimento = new Entidade("#" + this.tempo, this.tempo);
                        this.chegaram.add(this.emAtendimento);
                        this.emAtendimento.setHoraInicioAtendimento(this.tempo);
                    }
                    contTec = 0;
                } else {
                    break;
                }
            }
            
            if (contTs == ts) {
                this.finalizarAtendimento();
                this.atenderProximoDaFila();
                ts = (int)obterTempo(paramsTs);
                contTs = 0;
            }
            
            this.mostrarRelatorio();
            
            this.parada = this.deveParar(tempoLimite);
            this.tempo++;
            contTec++;
            if (this.emAtendimento instanceof Entidade) 
                contTs++;
        }
        
    }
    
    public boolean podeCriarNovaEntidade() {
        if (totalEntidadesSistema() +1 > this.capacidadeSistema) {
            System.out.println("\n\nERRO:A capacidade máxima do sistema é \"" 
                    + this.capacidadeSistema + "\".");
            this.parada = true;
            return false;
        }
        return true;
    }
    
    public boolean podeCriarNovaEntidadeNaFila() {
        if (this.tamanhoMaximoFila == 0) {
            return true;
        } else {
            if (this.fila.size() +1 > this.tamanhoMaximoFila) {
                System.out.println("\n\nERRO:A capacidade máxima da fila é \"" 
                        + this.tamanhoMaximoFila + "\" no tempo "+ this.tempo +".");
                this.parada = true;
                return false;
            }
        }
        return true;
    }
    
    
    public void finalizarAtendimento() {
        System.out.println("Finalizando atendimento do \"" + this.emAtendimento.getIdentificador() + "\"");
        this.emAtendimento.setHoraFinalAtendimento(this.tempo);
        this.emAtendimento.setHoraSaida(this.tempo);
        this.sairam.add(this.emAtendimento);
        this.emAtendimento = null;
    }
    
    public void atenderProximoDaFila() {
        if (this.fila.size() > 0) {
            this.emAtendimento = this.fila.remove(0);
            this.emAtendimento.setHoraInicioAtendimento(this.tempo);
            System.out.println("Iniciando atendimento do \"" + this.emAtendimento.getIdentificador() + "\"");
        }
    }
    
    public boolean deveParar(int tempoLimite){
        return this.tempo == tempoLimite;
    }
    
    public int totalEntidadesSistema() {
        int total;
        if (this.emAtendimento instanceof Entidade)
            total = this.fila.size() + 1;
        else
            total = this.fila.size();
        
        if (total > this.quantidadeMaximaEntidadeSistema)
            this.quantidadeMaximaEntidadeSistema = total;
        return total;
    }
    
    public double calcularTempoMedioEntidadeFila() {
        double total = 0;
        double quantidade = 0;
        for (int i=0; i<this.sairam.size(); i++) {
            if (sairam.get(i).getTempoFila() != 0) {
                total += sairam.get(i).getTempoFila();
                quantidade++;
            }
        }
        if (quantidade > 0)
            return (double)total/quantidade;
        else
            return 0;
    }
    
    public double calcularTempoMedioEntidadeSistema() {
        double total = 0;
        double quantidade = 0;
        for (int i=0; i<this.sairam.size(); i++) {
            total += this.sairam.get(i).getTempoSistema();
            quantidade++;
        }
        if (quantidade > 0)
            return (double)total/quantidade;
        else
            return 0;
    }
    
    public double calcularTaxaOcupacaoServidor() { //não sei se ta certo.
        if (this.tempo > 0 && (this.sairam.size()/this.tempo) != 0) {
            return (double)((this.chegaram.size()/this.tempo) / (this.sairam.size()/this.tempo));
        }
        return 0;
    }
    
    public double calcularNumeroMedioEntidadesFila() {
        System.out.println("nao sei como fazer isso.");
        return 0;
    }
    
    public void mostrarRelatorio() {
        System.out.println("");
        if (this.emAtendimento instanceof Entidade)
            System.out.println("Em atendimento: " + this.emAtendimento.getIdentificador());
        else 
            System.out.println("Em atendimento: ");
        mostrarLista();
        System.out.println("Total de entidades no sistema: " + totalEntidadesSistema());
        System.out.println("Total de entidades máxima no sistema: " + this.quantidadeMaximaEntidadeSistema);
        System.out.println("Quantidade entidades que chegaram: " + this.chegaram.size() + "");
        System.out.println("Quantidade entidades que saíram: " + this.sairam.size() + "");
        System.out.printf("Tempo médio de entidade na fila: %.5f\n", calcularTempoMedioEntidadeFila());
        System.out.printf("Tempo médio de entidade no sistema: %.5f\n", calcularTempoMedioEntidadeSistema());
        
        // TODO revisar a taxa de ocupacao
        System.out.printf("Tempo medio de taxa de ocupação do servidor: %.5f\n", calcularTaxaOcupacaoServidor());
    }
    
    public void mostrarLista() {
        System.out.print("Fila(" + this.fila.size() + "): ");
        for (int i=this.fila.size()-1;i>-1;i--) {
            System.out.print("[" + this.fila.get(i).getIdentificador() + "] ");
        }
        System.out.println("");
    }
    
    public int obterTempo(Map<String,String> params) {
        if (params.containsKey("valor")) {
            return Integer.parseInt(params.get("valor"));
        } else {
            switch(params.get("dist")) {
                case "uniforme":
                    System.out.println((int)new GeradorNumeroAleatorio().uniforme(
                            Double.parseDouble(params.get("minimo")),
                            Double.parseDouble(params.get("maximo"))));
                    return (int)new GeradorNumeroAleatorio().uniforme(
                            Double.parseDouble(params.get("minimo")),
                            Double.parseDouble(params.get("maximo")));
                case "triangular":
                    return (int)new GeradorNumeroAleatorio().triangular(
                            Double.parseDouble(params.get("minimo")), 
                            Double.parseDouble(params.get("maximo")), 
                            Double.parseDouble(params.get("moda")));
                case "exponencial":
                    return (int)new GeradorNumeroAleatorio().exponencial(
                            Double.parseDouble(params.get("media")));
                case "normal":
                    return (int)new GeradorNumeroAleatorio().normal(
                            Double.parseDouble(params.get("media")),
                            Double.parseDouble(params.get("variancia")));
            }
        }
        return 0;
    }
}
