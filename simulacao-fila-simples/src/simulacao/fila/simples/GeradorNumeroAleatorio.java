package simulacao.fila.simples;

public class GeradorNumeroAleatorio {
    
    public double uniforme(double minimo, double maximo) {
        return minimo + (maximo - minimo) * Math.random();
    }
    
    public double triangular(double minimo, double maximo, double moda) {
        double aleatorio = Math.random();
        if (((moda - minimo)/(maximo - minimo)) > aleatorio) {
            return minimo + Math.sqrt(aleatorio*(moda - minimo)*(maximo - minimo));
        } else {
            return maximo - Math.sqrt((1 - aleatorio)*(maximo - moda)*(maximo - minimo));
        }
    }
    
    public double exponencial(double media) {
        return -media * Math.log(Math.random());
    }
    
    public double normal(double media, double variancia) {
        double z = Math.sqrt(-2*Math.log(Math.random())) * 
                Math.sin(2*Math.PI*Math.random());
        return media + variancia*z;
    }
    
    public double continuo(double number) {
        return number;
    }
}
